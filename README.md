This is the supplementary codes for the manuscript "Myocardial ATP Depletion Detected Noninvasively Predicts Sudden Cardiac Death Risk in Heart Failure Patients" by T. Jake Samuel et al. 

The codes simulate heart fialure with low ATP level and normal ATP level using the ECME model (Cortassa et al. 2006).
