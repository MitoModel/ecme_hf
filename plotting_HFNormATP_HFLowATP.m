time1=load('voiHFnorm.mat');
time2=load('voiHFlow.mat');
state1=load('stateHFnorm.mat');
state2=load('stateHFlow.mat');
alge1=load('algeHFnorm.mat');
alge2=load('algeHFlow.mat');

figure;
V1=state1.STATES(:,2);
V2=state2.STATES(:,2);
T1=time1.VOI(:,1)/1000;
T2=time2.VOI(:,1)/1000;
plot(T1,V1,'k', T2,V2,'--k')
xlabel('Time(s)');
ylabel('Action potential (mV)')
legend('HF:normal ATP','HF:low ATP')
ylim([-100 70])
xlim([19.45,19.75]) %xlim([18.9,20])

% figure;
% plot(T1,state1.STATES(:,47),'k',T2,state2.STATES(:,47),'--k' ,T1,state1.STATES(:,48),'r',T2,state2.STATES(:,48),'--r', T1,state1.STATES(:,49),'g',T2,state2.STATES(:,49),'--g' ,T1,state1.STATES(:,1),'b',T2,state2.STATES(:,1),'--b')
% title('CrPi mito(black), CrPi cyto(red), ATPi cyto(green), ATPi(blue)')
% legend('HF:normal ATP','HF:low ATP')


% figure;
% plot(T1, alge1.ALGEBRAIC(:,134),'k', T2, alge2.ALGEBRAIC(:,134),'--k',T1, alge1.ALGEBRAIC(:,129),'b', T2, alge2.ALGEBRAIC(:,129),'--b' ,T1, alge1.ALGEBRAIC(:,130),'r', T2, alge2.ALGEBRAIC(:,130),'--r' )
% %legend('IKs', 'IK1', 'IKp')
% title('IKs(black), IK1(blue), IKp(red)')
% legend('HF:normal ATP','HF:low ATP')
% xlim([18.9,20])
% xlabel('Time(s)');
% ylabel('current (uA/uF)') ;

% figure;
% plot(T1, alge1.ALGEBRAIC(:,36),'k', T2, alge2.ALGEBRAIC(:,36),'--k',T1, alge1.ALGEBRAIC(:,122),'b', T2, alge2.ALGEBRAIC(:,122),'--b' ,T1, alge1.ALGEBRAIC(:,46),'r', T2, alge2.ALGEBRAIC(:,46),'--r' )
% title('ICa(black, L-type Ca2+), INa(blue), INaCa(red)')
% legend('HF:normal ATP','HF:low ATP')
% xlim([18.9,20])
% ylim([-15,5])
% xlabel('Time(s)');
% ylabel('current (uA/uF)') ;



% figure;
% plot(T1, alge1.ALGEBRAIC(:,40),'k', T2, alge2.ALGEBRAIC(:,40),'--k',T1, alge1.ALGEBRAIC(:,48),'r', T2, alge2.ALGEBRAIC(:,48),'--r' )
% title('INaK(black, Na+/K+ ATPase),  IpCa(red, sarcolemmal Ca2+ ATPase)')
% legend('HF:normal ATP','HF:low ATP')
% xlim([18.9,20])
% xlabel('Time(s)');
% ylabel('current (uA/uF)') ;


plot(T1, alge1.ALGEBRAIC(:,100),'k', T2, alge2.ALGEBRAIC(:,100),'--k')
title('Jup(black, SERCA)')
legend('HF:normal ATP','HF:low ATP')
xlim([18.9,20])
ylabel('flux (mM/ms)') ;
xlabel('Time(s)');



figure;
plot(T1,state1.STATES(:,15),'k',T2,state2.STATES(:,15),'--k')
legend('HF:normal ATP','HF:low ATP')
title('cytosolic Ca2+')
ylabel('Cai(mM)'); %('current (uA/uF)') ;
xlabel('Time(s)');
xlim([18.9,20])


